---
layout: page
current: about
title: About
navigation: true
logo: 'assets/images/ghost.png'
class: page-template
subclass: 'post page'
---

# lab.sub

This blog is maintained by the technical staff at Göttingen State and University
Library. There is no restriction to subject, article length or the
like. New posts will cover topics like fancy new programming languages, data
science and sources, open source software and many more.

Authors may maintain a profile page and work more or less with tags to add
ultracool metadata to their articles.

The blog is published with <a href="https://jekyllrb.com/">Jekyll</a>, the theme is based on <a href="https://github.com/myJekyll/jasper2">Jasper2</a>.

It does not contain any JavaScript and only static pages are served.
