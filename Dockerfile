FROM ruby:3.3 AS build

ENV JEKYLL_ENV=production

WORKDIR /usr/src/app

COPY . /usr/src/app

RUN bundle install && \
    bundle exec jekyll build -d public

FROM caddy:2-alpine

COPY --from=build /usr/src/app/public/ /usr/share/caddy/
