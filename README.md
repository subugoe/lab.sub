# lab.sub

## new authors
please register at [`_data/authors.yml`](_data/authors.yml).
You can add a gravatar id there (for example 6cfefa5456ac7fd4f39298f29c05fae8), but also add a profile image to `assets/images/authors/` and put the
name of the image into the YAML file (`image: ipf.jpg`)

## new posts
create a markdown file in the `_posts` directory. you have to use the filename
template `yyyy-mm-dd-title.md`. the `title` part will be resource name in the
url `/title.html`.

in the markdown file you have to use a front matter (header) like in the
[welcome.md](_posts/2017-01-01-welcome.md).

for customized styles you can edit the `subclass` value.

### DOI

The DOI can be retrieved, once the article is published. Head over to https://rogue-scholar.org/blogs/lab_sub and copy the
DOI from the article to the front matter of the new article.

## tags
Everyone is free to define new tags. All tags visible via `tags.html`. Further
specifications of a tag may be added to [data file](_data/(tags.yml)). The cover
image is optional but if not specified a file with tag-TAGNAME.png is used if
present.

## attachments
You can add attachments to your posts. Just add `attachments: [comma separated file names]`
to the front matter of your post and upload the files to the `attachments` directory.

## local preview
```bash
bundle install
bundle exec jekyll serve
```

## local preview with docker-compose

```bash
docker-compose up -d
```
Wait a little time and then open the browser and point it to http://localhost:4000. When you are changing posts or whatever, an automatic rebuild is triggered.

## local preview with docker

Run

* `docker build -t lab .`
* `docker run -it --rm --name lablab -p 4000:80 lab`

and open the site at [http://localhost:4000](http://localhost:9878).


## customizations
### SCSS
Add SCSS files / modifications to `/_sass/`

### images
add your images to `assets/images`.

## template
Jasper2: http://jekyllthemes.org/themes/jasper2/

### fork
from: https://github.com/myJekyll/jasper2

to: https://gitlab.gwdg.de/subugoe/lab.sub
