---
layout: post
current: post
cover: assets/images/Bibliothek_der_UNI_Goettingen-wikiCommons-Suhaknoke.webp
cover-credits: CC BY-SA 4.0 WikiCommons:Suhaknoke
navigation: True
title: Developers wanted (PHP, Java, TYPO3, Frontend, Symfony, Go, ...)
date: 2022-11-01 08:44:44
tags: goettingen
class: post-template
subclass: 'post job'
author: ipf
doi: https://doi.org/10.59350/12hk8-rxg98
---

We are looking for developers who fit into our team - so if you want to work in a great work environment in a beautiful city (Göttingen) and are an experienced software developer, please apply. 

We are working with a wide range of technologies, so we hope, there's something for you.

For more information, please visit [www.sub.uni-goettingen.de](https://www.sub.uni-goettingen.de/wir-ueber-uns/stellenangebote-ausbildung/stellenangebot/software-engineer-all-genders-welcome-e-13-tv-l-full-time-limited/). If we can help you in any way or answer your questions, please use the contact details found on the above website!
