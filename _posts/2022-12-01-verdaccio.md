---
layout: post
current: post
cover: assets/images/verdaccio.webp
cover-credits: The Virgin and Child with Saint John and Angels (1497), National Gallery, London
navigation: True
title: "Introducing a lightweight Node.js proxy registry service: Verdaccio"
date: 2022-12-01 13:56
tags: ['node-js', 'dependencies']
class: post-template
subclass: 'post verdaccio'
author: hynek
doi: https://doi.org/10.59350/ba3yd-sj178
---

Using a dependency proxy with npm, yarn or pnpm in CI or local development can greatly reduce outgoing traffic. Fortunately, FE now maintains an instance of such service.

## Verdaccio – not only a classical underpainting technique

> Verdaccio is a simple, zero-config-required local private NPM registry. No need for an entire database just to get started. Verdaccio comes out of the box with its own tiny database, and the ability to proxy other registries (eg. npmjs.org), also introduces caching the downloaded modules along the way. For those who are looking to extend their storage capabilities, Verdaccio supports various community-made plugins to hook into services such as Amazon's S3, Google Cloud Storage or create your own plugin.
([verdaccio.org](https://verdaccio.org/))

The project maintains [a current Helm chart](https://github.com/verdaccio/charts) that made its deployment into a Kubernetes cluster work like a charm.

## How to set it up

In order to use our dependency proxy service, you have to add its registry to your project's npm configuration. Issue the following command in the directory, where your project's `package.json` resides:

```sh
echo registry=https://npm.sub.uni-goettingen.de/ >> .npmrc
```

This is it. — Yes, really!

For your convenience, we also have configured Verdaccio to fetch packages from [GWDG GitLab](https://gitlab.gwdg.de) registries for the two scopes used in the SUB: `@fe/*` and `@subugoe/*`.

## FAQ

### What if the service is down/not reachable?

In the unlikely event that the service is not at your disposal, remove the line `registry=https://npm.sub.uni-goettingen.de/` from the `.npmrc` file and proceed as usual.
