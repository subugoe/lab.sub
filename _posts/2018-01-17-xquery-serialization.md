---
layout: post
current: post
cover: assets/images/tag-xquery.webp
cover-credits: »Breezeicons-actions-22-xml-node-delete.svg« © 2014 Andreas Kainz et.al. / KDE, via Wikimedia Commons, LGPL, altered
navigation: True
title: XQuery Serialization Options
date: 2018-01-17 00:00:01
tags: 
  - xquery
  - xpath
class: post-template
subclass: 'post tag-xquery'
author: mathias
doi: https://doi.org/10.59350/emnvc-4af28
---
Here is a cheat sheet to [XQuery serialization](https://maxtoroq.github.io/xpath-ref/fn/serialize.html).
