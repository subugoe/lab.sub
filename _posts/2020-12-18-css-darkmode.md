---
layout: post
current: post
cover: assets/images/darkmode.png
cover-credits: 
navigation: True
title: Darkmode in CSS
date: 2020-12-18 04:44:44
tags: css
class: post-template
subclass: 'post css-darkmode'
author: ipf
doi: https://doi.org/10.59350/r5rdk-vj079
---
You may know the dark mode from your mobile phone or elsewhere. It's basically
a theme that can be activated, but also can be a system-wide default.

Most browsers do also [support](https://caniuse.com/prefers-color-scheme) this setting -
and we as web developers can read these preferences ([Firefox](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme)) and 
use media queries to apply a dark theme for our website.

Note that this is not togglable, as some JavaScript solutions offer this feature, but only reacting
to the browser preference. By the way - I removed *all* JavaScript from this blog.

I did it for this blog, and all I had to do, was to add a SCSS File containing a media query:

```scss
@media (prefers-color-scheme: dark) {
    body {
        background: #000;
        color: #fff;
    }
}
```

and also some other styles. Handling everything and giving it some polish will take
some time - but the basics are quite easy to achieve.
