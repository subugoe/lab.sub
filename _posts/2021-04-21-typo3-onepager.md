---
layout: post
current: post
cover: assets/images/gay-pride-flag.png
cover-credits: Public domain, via Wikimedia Commons
navigation: True
title: Creating a onepager website with TYPO3
date: 2021-04-21 04:42:00
tags: ['typo3']
class: post-template
subclass: 'post t3doc'
author: snaegle
doi: https://doi.org/10.59350/r6pff-rrj57
---

I wanted to switch a small website into a one-pager website lately, and as it so happend - the site was managed with TYPO3. Turns out, with TYPO3 it's a piece of cake. 

I already had a sitepackage and a template, so the only thing was to glue the pages in a string, make them distingusable from each other, and adjust the menu.


# Taking care of the glue

The pages can be glued together with two viewhelpers from the vhs-extension. With `<v:page.menu pageUID="1">` we get all the subpages of our root page, and with `<v:content.render pageUID="x">` we can get the content of a specific page. No need for any declaration of `styles.content.get` in the Fluidtemplate anymore.

```
<main>
    <v:page.menu pageUid="1" as="page">
        <f:for each="{page}" as="pageObject">
            <v:content.render pageUid="{pageObject.uid}"/>
        </f:for>
    </v:page.menu>
</main>
```

## Customizing the order of the pages
In case we are not happy with the automated succession of our pages according to their uids, we can also define our own order. 
```
<f:for each="{
    0:page.2,
    1:page.6,
    2:page.7,
    3:page.3,
    4:page.10,
    5:page.4}" as="pageObject">
            <v:content.render pageUid="{pageObject.uid}"/>
</for>
```
## Defining different layouts
To distinguish the single pages from one another it is good to provide them with different layouts, for example backgound colors or images. 

One way to do this is to choose a frontend layout in the configuration for each page in the backend, and ask for it in our template via `{pageObject.layout}`. To customize the labeling of the layout dropdown we can use this code:
```typoscript
TCEFORM.pages.layout {
    removeItems = 1,2,3
    altLabels.0 = None
    addItems {
        10 = Image
        20 = Blank
    }
}
```

Another way is to create alternating layouts is to ask if we have to style an even or an odd page:
```
<f:for each="{page}" as="pageObject" iteration="pageiterator">
    <div class="layout-{f:if(condition: pageiterator.isOdd, then:'0', else:'1')}">
        <v:content.render pageUid="{pageObject.uid}"/>
    </div>
</for>
```
which gives you alternating layouts like this:
```
<div class="layout-1">
    <v:content.render pageUid="{pageObject.uid}"/>
</div>
<div class="layout-0">
    <v:content.render pageUid="{pageObject.uid}"/>
</div>
```
# Taking care of the menu
The menu is quite easy. There only have to be anchors on top of each page section, and links in the menu to these anchors. They both can use `{pageObject.title}` as matchmaker.

In the main template:
```
<f:for each="{page}" as="pageObject">
   <a name="{pageObject.title}"></a>
        <v:content.render pageUid="{pageObject.uid}"/>
    </div>
</for>
```
and in the header:
```
<ul>
    <f:for each="{page}" as="pageObject">
        <li><a href="#{pageObject.title}">{pageObject.title}</a></li>
    </for>
</ul>
```

And that's it already, all set :)
