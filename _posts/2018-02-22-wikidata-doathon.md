---
layout: post
current: post
cover: assets/images/Datamodel_in_Wikidata.svg.webp
cover-credits: By User:Charlie_Kritschmar_(WMDE),CC0,https://commons.wikimedia.org/wiki/File:Datamodel_in_Wikidata.svg
navigation: True
title: Wikidata Do-A-Thon
date: 2018-02-22 11:59:01
tags: 
  - wikidata
class: post-template
author: friedrichmueller
doi: https://doi.org/10.59350/6d9ay-yjy33
---

A [Do-A-Thon event](https://www.github.com/goe-wikidata/doathon-20180214) took place at the SUB on the 14th and 15th February that brought members from the Göttingen Campus and the [Wikidata](https://www.wikidata.org) community together in order to dive into the world of Wikidata and pilot projects.

For a research library a knowledge base like Wikidata should be of particular interest. On the one side libraries can publish structured data with Wikidata and on the other side content from Wikidata can be re-used within research projects.

# Wikidata?

Wikidata is a source of human and machine-readable, multilingual, structured data published under the CC0 license. Unlike other projects e.g. [DBpedia](https://wiki.dbpedia.org/) that primarily aim to provide structured data from Wikimedia projects, Wikidata has recently emerged as a user curated knowledge base that can be read and edited by everyone.
Wikidata also supports Wikimedia projects e.g. Wikipedia with information boxes and links to other languages. The idea is here to have a centralized source that feeds the different Wikimedia projects with common used data e.g. locations, dates and statistics.

# A glance at Wikidata 

#### Everything starts with an item

The data is structured into items that are uniquely identified by a Q followed by a number (e.g. the SUB has the identifier [Q564783](https://www.wikidata.org/wiki/Q564783) ). Each item has certain attributes like a label and is further characterized through statements that are expressed with a property and value.

For example:

| Item | Property | Value |
| ------ | ------ | ------ |
| Göttingen State and University Library | part of | University of Göttingen |
| [Q564783](https://www.wikidata.org/wiki/Q564783) | [P361](https://www.wikidata.org/wiki/Property:P361) | [Q152838](https://www.wikidata.org/wiki/Q152838) |


Further important terms you can see in the picture below:

![package settings view](assets/images/Datamodel_in_Wikidata.svg.webp)


Besides collecting just statements Wikidata also records further information e.g. the sources of the statements and the connections to other databases. This allows to explore the data distribution and supports the verifiability of data. 


#### More information

See [Introduction](https://www.wikidata.org/wiki/Wikidata:Introduction) and [Tutorials](https://www.wikidata.org/wiki/Wikidata:Tours).

#### Retrieve data

Data can be accessed in [different ways](https://www.wikidata.org/wiki/Wikidata:Data_access) e.g. 
through the [SPARQL endpoint](https://query.wikidata.org/bigdata/namespace/wdq/sparql?query={SPARQL}) and the [Wikidata Query Service GUI](https://query.wikidata.org).
The Wikidata Query Service GUI supports the user with a query helper that allows to build queries without using SPARQL. 

To get familiar with the data structure it is a good way to browse through items and discover the different statements and identifiers.
Wikidata delivers also a bunch of [example queries](https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples) you can lean on. 

#### Tools

There are already different packages and [tools](https://www.wikidata.org/wiki/Wikidata:Tools/de) available.


#### Development

If you plan to integrate Wikidata functionality into your applications SDKs are available like the [Wikidata SDK](https://github.com/maxlath/wikidata-sdk) for Javascript.
Also the application of the [Wikidata-query-gui](https://github.com/wikimedia/wikidata-query-gui-deploy) can be re-used and adapted to your project requirements.




# Outcomes of the Do-A-Thon

Three [project ideas](https://github.com/goe-wikidata/doathon-20180214/issues) have been presented:


1. Evaluating the possibilities of using Wikidata as a data source for a 'Aulus Gellius' Noctes Atticae' project.


2. WD2GB (Wikidata to [Geo-Browser](https://geobrowser.de.dariah.eu/)) 
An application that allows to query Wikidata with SPARQL - Reuse your data with the DARIAH-Storage-Service and explore spatio-temporal relations with the Geo-Browser.

    * Github: [https://github.com/FriedrichMueller/WD2GB](https://github.com/FriedrichMueller/WD2GB)
	
    * Demo: [https://mueller273.pages.gwdg.de/WD2GB](https://mueller273.pages.gwdg.de/WD2GB)
	

3. Populating and interlinking publications from a Life Science Research Center
Making all Göttinger SFB 1002 papers available with Wikidata.
	* [https://www.wikidata.org/wiki/Q48693816](https://www.wikidata.org/wiki/Q48693816)



	
Since the official start in 2012 Wikidata increased to over  44 million items at the time of writing but still a lot of issues have to be [discussed](https://www.wikidata.org/wiki/Wikidata:Project_chat) and [work](https://www.wikidata.org/wiki/Wikidata:WikiProjects) has to be done.

Try Wikidata-maybe it will be a companion in your next project.
