---
layout: post
current: post
cover: assets/images/oldschool.webp
cover-credits: BBy Kuba Bożanowski from Warsaw, Poland (Oldschool) CC BY 2.0 (https://creativecommons.org/licenses/by/2.0), via Wikimedia Commons
navigation: True
title: (S)FTP and Gitlab CI 
date: 2018-02-28 07:42:00
tags: 
  - tools
  - jekyll
class: post-template
subclass: 'post oldschool'
author: ipf
doi: https://doi.org/10.59350/ccf0r-yay20
---
Sometimes the most sophisticated deployment strategies and infrastructure models can unfortunatly not be shifted into the real life, so
one has to go back a few decades and use tools from that time and mix them with current ones.

In a former [post]({% post_url 2018-02-21-jekyll-docker %}) I described a cool solution for building a static HTML website and putting it into
a Docker image. This method could not be used for serving the blog (don't ask about details …), so I was forced to use a
shared Webhosting that runs a webserver and multiple websites at once.

So far, so good, nothing wrong with that, but the crux of matter is, that the only deployment (a.k.a copying the build artifacts)
solution provided by the hoster is SFTP. I cannot really remember, if I ever used SFTP, because most times back in the days
one used FTP or SCP. SCP was not available, because SSH is not installed on the webserver, and we don't need to talk about 
using FTP nowadays.

As the code of this blog resides in a Gitlab repository and we do have some experience with Gitlab CI, AND we wanted the
sites to be deployed automatically on change the task was to take the built HTML files and copy them via SFTP to the webserver.

I don't want to put usernames and passwords and sensitive information in a code repository, so first of all, some variables
for that CI tasks need to be defined, such as `SFTP_USERNAME`, `SFTP_PASSWORD`, `WEBSERVER` and `PORT`.

After googling around for a while, I found a scriptable SFTP-Client that completely fits my needs for the task: [lftp](https://lftp.yar.ru/).

As seen below, the deployment uses a [Ruby Docker image](https://hub.docker.com/_/ruby/), installs all dependencies in the 
`before_script` section, builds the static HTML website, does some SSH weakening and finally copies the build artifacts
to the final destination.

```yaml
image: ruby:2.3

variables:
  JEKYLL_ENV: production

before_script:
- bundle install

oldschool:
  stage: deploy
  script:
  - bundle exec jekyll build -d public
  - apt-get update -yqq && apt-get install -yqq lftp
  - 'which ssh-agent || ( apt-get install openssh-client -y )'
  - mkdir -p ~/.ssh
  - eval $(ssh-agent -s)
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
  - lftp -c "mirror -R public/ sftp://$SFTP_USERNAME:$SFTP_PASSWORD@$WEBSERVER:$PORT/www/lab"
  artifacts:
    paths:
    - public
```
