---
layout: post
current: post
cover:  assets/images/website-g25f37a619_640.webp
cover-credits: Pixabay Lizenz (https://pixabay.com/de/service/terms/#license)
navigation: True
title: How to simplify the familiarization with the code
date: 2023-03-14 00:00:01
tags: ['howto'] 
class: post-template
subclass: 'post howto'
author: jpanzer
doi: https://doi.org/10.59350/45tk2-rg538
---

> Getting involved in an ongoing or completed project is time-consuming. Creating good documentation (README and developer documentation) is not easy, time-consuming and usually questions remain unanswered and implementation details remain hidden. Ideally, members of the development team are available and can be asked. However, it is not uncommon that there is no access to the team anymore and even if this is given, it is not guaranteed that the developers can answer ad-hoc questions. In this respect, the question is how to counteract this situation.

### Conducting debug sessions

In our teams, we have repeatedly conducted debug sessions to help team members get started with the development. An entry point is pointed out and the control and data flow is followed together. At important code points breakpoints are set and the state (variable values) is inspected.  

The whole thing can be recorded as a video cast, e.g. with BBB and is available for recapitulation or for others.

If a debug session is performed at a time when code is actively being worked on, the goal is usually reached after 10-15 minutes. For a comparable result, many hours are needed for documentation, which in the end does not reach nearly the level of detail as a debug session. The most important thing the session should do is to explain how to start the application locally (e.g. from the development environment) and where to set the first breakpoints. The rest can then be explored iteratively on your own. In the end, it is not possible to cover all paths anyway.

For example, to debug the DigiZeitschriften Indexer, I start the docker-based environment (Redis, ElasticSearch, local S3 installation, Web API and Kibana). Then I set (at least) one breakpoint, e.g. somewhere in the main. I work with VS Code, here it is necessary to create a file launch.json, e.g. to include environment variables or directories (for details see the VS Code documentation). A click on the icon "Run and debug" starts the application and if it doesn't exit before the breakpoint, e.g. in the init(), the debugger should wait for instructions at the breakpoint, e.g. step-in, step-over, etc. The nice thing is that variable values can be inspected or that it is possible to step into included libraries in case of an exception to see where and why something is not running.

With a little more investigation it is also possible to debug applications remotely.

I wish you a lot of fun trying it out and I'm looking forward to your feedback if you find this approach useful and if you can imagine to run a debug session yourself :)
