---
layout: post
current: post
cover: assets/images/tag-documentation.webp
cover-credits: By David Whelan - https://www.flickr.com/photos/davidpwhelan/30640643420/, CC0, https://commons.wikimedia.org/w/index.php?curid=58021850
navigation: True
title: How to write a self-updating documentation
date: 2018-01-25 10:00:01
tags: 
  - documentation
class: post-template
subclass: 'post tag-self-updating-docs'
author: dennis
doi: https://doi.org/10.59350/f1j2w-aas05
---

Well, at least partially self-updating. You still will have to write the sentences and all that…

Suppose you are at the beginning of a project and have written some classes. As a responsible developer, you want to write a textual documentation, in which you explain what the classes do, when and how they are started, how they are connected to each other, and so on.

The easiest way is to write a text file beginning like this:

```
Web Entry Point

The entry point of the application is the WebApplication class.
It has a main method that starts a Spring Boot application.
```

The main trouble with this kind of documentation is that it is a bad idea to use concrete class names, like WebApplication. Especially at the beginning of a project, the probability is high that the names will be changed, either by yourself, or by someone on your team. One solution would be to always avoid the names and for example to say ‚the class that starts the web application‘.

One other possible solution is our topic. In Java, the compiler knows all the class names and makes sure that they are always in sync. (In other languages, it is the interpreter). How can we make the compiler manage the class names inside our documentation? 

Instead of the text file, we can create an equivalent Java class file. It consists of correct Java syntax, but it contains our documentation. It is never supposed to be executed in our application. It simply is located next to our source code where the compiler can reach an check it. (Actually, the compiler builds it, but it does not hurt the application). Our example from above could look like this:

```java
class WebModuleDocumentation {

  void webEntryPoint() {
    /*
     * The entry point of the application is */ the(WebApplication.class); /*.
     * It has a main method that starts a Spring Boot application.
     * 
     */
  }

}
```

This is a syntactically correct Java class with comments that contain our documentation. In between, there is a snippet of Java code that does nothing besides referencing a name of a production class. The method the() can be defined in the same or in another class:

```java
  static void the(Class<?> cl) {
    // doing nothing
  }
```

In a similar fashion, also methods from the production code can be referenced. 

In comparison to the text format (or Wiki, or PDF…), such documentation classes have some advantages:

- You can refactor all class and method names in your code and they will be updated in the documentation automatically.

- If a class or a method in the production code is deleted, there will be a compile error in the documentation.

- Inside of an IDE, you can easily navigate to the referenced classes and methods while reading the documentation.

A real example can be seen at [https://github.com/subugoe/solr-importer/blob/master/web/src/main/java/docs/WebModuleDocumentation.java](https://github.com/subugoe/solr-importer/blob/master/web/src/main/java/docs/WebModuleDocumentation.java)
