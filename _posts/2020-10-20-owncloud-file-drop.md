---
layout: post
current: post
cover: assets/images/typewriter-doll.webp
cover-credits: https://unsplash.com/photos/HbbHfXvb6Xw
navigation: True
title: Upload to ownCloud without auth*
date: 2020-10-20 04:44:44
tags: owncloud API
class: post-template
subclass: 'post owncloud'
author: mathias
doi: https://doi.org/10.59350/w1r0s-bkm12
---

I really like the *file drop* feature of ownCloud. You can configure a
directory to receive files from a thrid party without authentication and
with no troubles. You get a URL and that is everything you have to share.
People will see a simple file upload field and can push their files
directly into your ownCloud. Easy.

When you want a machine to upload to this directory, your browsers
console will send a more complex request, with access tokens, session id
and all the stuff you will need an inadequate amount of time to reproduce
with your code.

Then I found the solution posted [here in 2018](https://blog.happyz.me/2018/upload-file-to-nextcloud-owncloud-programmatically/) that is still working.

A file drop URL usually looks like `https://yourowncloud.de.vu/index.php/s/TrQB1MeB5pf5rZ`.
Take the last part as a username for a HTTP basic auth, and proceed like this:

```bash
FILE="[ReplaceWithFileInCurrentDir]"

curl "https://yourowncloud.de.vu/public.php/webdav/$FILE" \
    -u "TrQB1MeB5pf5rZ:" \
    --compressed \
    -X PUT \
    --upload-file $FILE \
    -H 'X-Requested-With: XMLHttpRequest'
```

Easy.

As always when using undocumented features like this: They are subject to be removed – or may be not.
