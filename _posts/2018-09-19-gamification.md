---
layout: post
current: post
cover: assets/images/flamingos.webp
cover-credits: By Helmut Peter Schreier, CC 0, https://pixabay.com/de/flamingos-v%C3%B6gel-tiere-pink-wasser-1205366/
navigation: True
title: Die Wissenschaft retten und dabei Spaß haben 
date: 2018-09-19 17:12:00
tags: 
  - gamification
class: post-template
subclass: 'post sustain'
author: athanasios
doi: https://doi.org/10.59350/fxscr-n7213
---
## Von Athanasios Mazarakis und Paula Bräuer

## Eine Teilprojektvorstellung unseres Partners CAU Kiel zum Projekt OA-FWM

Das vom BMBF geförderte Projekt [„Fachspezifische OA-Workshops und Bereitstellung von OA-Materialien - OA-FWM“](https://open-access.net/ueber-uns/oa-fwm/) zielt darauf ab, insbesondere die fachspezifische Information und Ansprache im Bereich Open Access zu verbessern. Das Projekt ist eine Kooperation zwischen dem Kommunikations-, Informations-, und Medienzentrum KIM der Universität Konstanz, der Christian-Albrechts-Universität zu Kiel (CAU) und der Niedersächsischen Staats- und Universitätsbibliothek (SUB Göttingen).

Ein Hauptziel des Projektes ist es, Forschende als Multiplikatoren für die Verbreitung von Open Access zu gewinnen. Für deren Motivierung wird an der CAU in Kiel an Gamification geforscht. Das Konzept der Gamification beruht darauf, Spielelemente wie etwa Punkte, Bestenlisten und das Erreichen von Levels, als Anreizsystem in einem spielfremden Kontext einzusetzen. Es sollen außerdem fachspezifische Communities of Practice (CoP) etabliert werden, welche selbstorganisiert neue Inhalte auf der Internetplattform publizieren. Diese Wissensgemeinschaften sollen durch Forschende selbst gesteuert und organisiert werden.

Unter einer Community of Practice wird nach Wenger u.a. (2002, S. 4) eine Gruppe von Menschen, die ein Anliegen, eine Reihe von Problemen oder eine Leidenschaft für ein Thema teilen und ihr Wissen und ihr Fachwissen auf diesem Gebiet vertiefen, indem sie kontinuierlich interagieren, verstanden. 

Mitglieder einer CoP kommen zusammen, da sie gemeinsame Ziele und Interessen teilen. Neue Mitglieder arbeiten in diesen CoPs gemeinsam mit erfahreneren Mitgliedern, um ihre Fähigkeiten zu verbessern und werden nach und nach an schwierigere Aufgaben herangeführt.  

Damit diese CoP allerdings auch motiviert, kommt eine sogenannte Gamification zum Tragen. Hier versteht man darunter die Verwendung von Spielelementen in spielfremden Kontexten.  Gamification wird in vielen unterschiedlichen Bereichen eingesetzt, um Personen zu motivieren und ihr Engagement bzw. ihre Produktivität zu verbessern. Typische Spielelemente sind hierbei Punkte, Abzeichen oder auch Bestenlisten.

Um Gamification besonders effizient anwenden zu können, soll daher im Projekt OA-FWM untersucht werden ob und wenn ja welche Spielelemente besonders gut bei Mitgliedern verschiedener Fachrichtungen wirken. Die Motivation der Mitglieder aktiv zur Community beizutragen, soll durch die individuelle Anpassung der Spielelemente effektiv gefördert werden.

## Literatur

Wenger, E., McDermott, R. A., & Snyder, W. (2002). Cultivating communities of practice: a guide to managing knowledge. Boston, Mass: Harvard Business School Press.

Cox, A. (2005). What are communities of practice? A comparative review of four seminal works. Journal of Information Science, 31(6), 527-540.

Deterding, S., Dixon, D., Khaled, R., & Nacke, L. (2011). From game design elements to gamefulness: defining „gamification“. In Proceedings of the 15th international academic MindTrek conference: Envisioning future media environments (S. 9–15). ACM Press.
