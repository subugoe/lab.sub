---
layout: post
current: post
cover: assets/images/docker-rate-limit.webp
cover-credits: Basile Morin, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons
navigation: True
title: Gitlab Runner and Docker's Rate Limits
date: 2020-12-10 04:44:44
tags: ['docker', 'gitlab']
class: post-template
subclass: 'post docker-gitlab'
author: ipf
doi: https://doi.org/10.59350/nkmgp-0yd18
---
Docker activated "[rate limits](https://www.docker.com/increase-rate-limits)" by the end of 2020 - and as we are
frequently pulling images from the main docker registry, we ran into trouble and our builds failed.

But there is a quite simple solution for circumventing these rate limits - as we are using a gitlab runner on a dedicated
server. This server already has the docker daemon installed and the solution is already [documented](https://docs.docker.com/registry/recipes/mirror/). 

To build a docker pull-through cache registry, the most simple approach is, to have a local instance of
the docker registry, that caches the images that have not been pulled yet and delivers them from the cache.

To achieve this we also use docker - and the official [registry](https://hub.docker.com/_/registry) Image from docker hub.

The easiest way is to create a docker-compose manifest `docker-compose.yaml` with the following contents:

```yaml
version: '3'
services:
  registry:
    image: registry:latest
    ports:
      - '5000:5000'
    volumes:
      - ./config.yml:/etc/docker/registry/config.yml
    restart: always
```

and a config file `config.yml` that gets mapped to the container:

```yaml
version: 0.1
log:
  fields:
    service: registry
storage:
  cache:
    blobdescriptor: inmemory
  filesystem:
    rootdirectory: /var/lib/registry
http:
  addr: :5000
  headers:
    X-Content-Type-Options: [nosniff]
health:
  storagedriver:
    enabled: true
    interval: 10s
    threshold: 3
proxy:
  remoteurl: https://registry-1.docker.io
```

The most important part for the caching-feature is the `proxy` - section.

Then, in order to be used by the gitlab runner (here on the same server), the newly created registry
needs to be announced to the docker daemon.

So, put a reference to the new local registry in `/etc/docker/daemon.json`:

```yaml
{
  "registry-mirrors": ["http://localhost:5000"]
}
```

Restart the docker-daemon and start the registry-container with `docker-compose up -d` and you're done.

The docker executor from the gitlab runner will then first check the local registry that transparently forwards
and delivers images from the main registry.

The cached images are stored in a volume on the host - and are automatically cleaned up from time to time.
