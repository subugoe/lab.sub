---
layout: post
current: post
cover: assets/images/ghent.webp
cover-credits:  Ed Webster, Creative Commons Attribution 2.0 Generic license https://creativecommons.org/licenses/by/2.0/deed.en
navigation: True
title: IIIF Friday at Ghent University
date: 2022-10-13 08:44:44
tags: iiif
class: post-template
subclass: 'post iiif-friday'
author: ipf
doi: https://doi.org/10.59350/vhw6c-rmk66
---

On the 14th of october a 'IIIF-Friday' will be held at Ghent University. Most importantly, the major IIIF-Viewers are being presented, followed by a Q&A session.

Feel free to register for the online event at [meemoo](https://meemoo.be/en/training-and-events/seventh-iiif-friday-on-14-october).
