---
layout: post
current: post
cover:  assets/images/podcast_2.webp
cover-credits: ""
navigation: True
title: Werte bei Scrum
date: 2024-01-30 00:00:01
tags: ['podcast', 'agile'] 
class: post-template
subclass: 'post podcast'
author: kay
doi: https://doi.org/10.59350/4kv7m-vhq83
---

Heute soll es um die Werte in Scrum gehen. Ich glaube, dass gelebte Werte in der Teamarbeit eine absolute Grundvoraussetzung für ein High Performing Team sind.

Zum Einstieg möchte ich mit ein paar kurzen einleitenden Worten zu den Scrum Werten beginnen. Wenn euch das neugierig macht, solltet ihr unbedingt den kurzen Methoden-Montag Podcast dazu anhören! Mit Jan und Florian stelle ich euch dann meine neue Idee zu den Werten in Scrum vor...

Für mich spielen Werte in der agilen Transformation eine ganz entscheidende Rolle. Scrum ist nicht nur ein Framework, das einen bestimmten Workflow beschreibt, sondern besteht auch aus Prinzipien und Werten. Ich finde, ohne die Werte fühlen sich die Events in Scrum (die Meetings) wie eine leere Hülle an. Mein Ziel ist es, den Mensch immer in den Mittelpunkt zu stellen. Wenn wir es schaffen, die Werte *Offenheit*, *Mut*, *Respekt*, *Fokus* und *Commitment* in unseren Teams zu leben, dann schaffen wir es auch das gesamte Team auf die agile Reise mitzunehmen.
Für mich als Scrum Master und Agile Coach ist es wichtig, dass die Teams über die Werte diskutieren, um sie erlebbar zu machen und damit ihren individuellen Zugang zu den Werten finden:

[Zur Podcast Episode mit Kay](https://plus.rtl.de/podcast/methoden-montag-7pgphbwyfhj94/werte-einfach-besprechen-mit-kay-liewald-t6ykmvpcz2849)