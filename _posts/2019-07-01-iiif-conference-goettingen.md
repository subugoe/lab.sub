---
layout: post
current: post
cover: assets/images/final-selection-goose-girl.webp
cover-credits: IIIF Conference Logo
navigation: True
title: IIIF Conference Göttingen
date: 2019-07-01 04:42:00
tags: ['iiif', 'conference']
class: post-template
subclass: 'post iiif-goettingen'
author: ipf
doi: https://doi.org/10.59350/f1mgz-spc70
---

Last week I attended the official 2019 IIIF Conference in Göttingen. It was maybe the hottest week of the year, so the first thing the IIIF Consortium bought, was a fan to get some fresh air for the audience.

## IIIF?

IIIF (Triple-eye-eff), the **I**nternational **I**mage **I**nteroparability **F**ramework is a set of [APIs](https://iiif.io/technical-details/) for having a standardized retrieval of [Images](https://iiif.io/api/image/2.1/), [Metadata](https://iiif.io/api/presentation/2.1/) [and](https://iiif.io/api/auth/1.0) [more](https://iiif.io/api/search/1.0).

An image can for example be referenced by using a URI with the following schema:

`{scheme}://{server}{/prefix}/{identifier}/{region}/{size}/{rotation}/{quality}.{format}`, a real world example 
[https://images.sub.uni-goettingen.de/iiif/image/gdz:PPN647751801:00000001/square/209,/0/default.jpg](https://images.sub.uni-goettingen.de/iiif/image/gdz:PPN647751801:00000001/square/209,/0/default.jpg) may be used to manipulate any of the parameters.

## 2019 IIIF Conference in Göttingen

The conference was held in Göttingen from June 24th to June 28th at the "Alte Mensa" and a bunch of estimated 200 attendees from all over the world came here.

### First Day: Showcases

The Conference's Monday started with welcoming from people involved in the IIIF Consortium and the organizers of the conference and afterwards some really interesting Showcases were [presented](https://iiif.io/event/2019/goettingen/showcase/).

The SUB Göttingen also had a slot there and showed the usage of IIIF in Libraries. As every Talk was time-boxed everything went in time and I got some really cool insights in what is going on in the IIIF Universe.

![mdo](assets/images/iiif-goettingen/20190624_112100.webp)

After the first day (and also the most other days), the participants could choose whether to attend a guided city tour in Göttingen or a guided library tour.

### Second Day: Workshops

#### Mirador Hands-on

Day Two was the workshop day with three parallel tracks and a morning and an afternoon session. I attended the Mirador 3 workshop, that showed some nice insights into the current state of the development.
A shiny new design, based on Google's material design was integrated into mirador and the Application was rewritten using React JavaScript Framework.
A release date was not issued, but it's definitely not to be expected in 2019.

I think there is still really a lot to do, it's heading in the right direction, but still misses some features our [TIFY](https://tify.sub.uni-goettingen.de/demo?manifest=https://manifests.sub.uni-goettingen.de/iiif/presentation/PPN235181684_0037/manifest) viewer already has, such as url-based routing and nested structures in a table-of-content view.

#### Creating, delivering and consuming IIIF Manifests within the Goobi Community

[Goobi](https://goobi.io/) is a workflow engine for digitization projects. As we in Göttingen are using it for scanning Books for the [Göttingen Digitization Centre (GDZ)](https://gdz.sub.uni-goettingen.de/) it was nearly a must-attend workshop for me.

The workshop was really good prepared, each attendant received an e-mail prior to the event with the instructions on how to get everything running. A zip archive was provided and assistance in installing or solving problems was quickly given before the workshop. The slides and introductions are open for everyon at [https://goobi.io/workshops/iiif2019/](https://goobi.io/workshops/iiif2019/).

Steffen started with an introduction and showed how to work with Goobi and provided some tasks, so the participants were able to reproduce each step.

After that Jan, the product manager of [Goobi-Viewer])(https://www.intranda.com/digiverso/goobi-viewer/goobi-viewer-overview/) showed how the IIIF integration works with Goobi and that there was a ready-made product for generating IIIF-Resources right after the digitization.

Finally, some tools around IIIF (Goobi IIIF downloader, IIIF Model) were shown, but failed to run somehow.

It was really interesting to see the first two parts of the workshop, so if you want to play around a little with goobi, feel free to download everything you need at the link provided above.

#### IIIF Consortium Members reception at the Orangery.

The day was concluded with a social event at the Orangery Göttingen where a barbecue and drinks were served in a sunny enironment.

### Third Day: Plenary session including lightning talks

The first *real* conference day started with a welcome speach by the president of the Göttingen University, Ulrike Beisiegel.

This day there was only one track, and the main topics were again, that people from different organizations showed what the achieved and implemented in IIIF. Some really exciting and entertaining sessions were held.

The ones I do remember being interesting were:

* [Using IIIF to Catalogue Ethiopian Binding Decoration](https://iiif.io/event/2019/goettingen/program/55/)
* [Storiiies Editor - Digital Storytelling Powered by IIIF](https://iiif.io/event/2019/goettingen/program/50/)
* [Digital Manuscripts Without Borders. A cross-collections search and discovery platform of manuscripts and rare books](https://iiif.io/event/2019/goettingen/program/15/)
* [Rhinos, Recitals and World Cup Finals- Storytelling through IIIF AV](https://iiif.io/event/2019/goettingen/program/40/)
* [Modelling multiple versions of a digital surrogate with IIIF](https://iiif.io/event/2019/goettingen/program/2/)

There were far more interesting ones, so have a look at the list of talks.
The programme for Wednesday can be found at [https://iiif.io/event/2019/goettingen/wednesday/](https://iiif.io/event/2019/goettingen/wednesday/).

![m2](assets/images/iiif-goettingen/20190627_091153.webp)

#### Evening Reception

In the evening the [Startraum Göttingen](https://www.startraum-goettingen.de/) was chosen as a social event location where Dinner and Drinks were provided and some interesting talks beside the conference emerged.

### 4th and 5th Day: Parallel sessions

#### Thursday

Thursday and was packed with sessions, and it was really hard to choose between the different tracks.

The following, interesting talks were worth attending:

* [Newspapers and IIIF: Moving from Implementation to Interaction](https://iiif.io/event/2019/goettingen/program/32/)
* [Cuban collaborative IIIF shared platform](https://iiif.io/event/2019/goettingen/program/25/)
* [Simple Access to Cultural Heritage Assets via ONB Labs](https://iiif.io/event/2019/goettingen/program/17/)
* [Integrated Data Visualization – the Challenges of Linked Open Data](https://iiif.io/event/2019/goettingen/program/11/)
* [Designing a user friendly IIIF based data aggregation platform](https://iiif.io/event/2019/goettingen/program/78/)
* [Combining TEI and IIIF in a Virtual Research Environment](https://iiif.io/event/2019/goettingen/program/71/)

![vre](assets/images/iiif-goettingen/20190627_153028.webp)

Unfortunatly one of the speakers didn't show up, so an interesting talk didn't take place.

#### Friday

The last conference day started with a slot of three tracks, the ones I attended and found quite interesting were

* [Making and Breaking Books: Reconstructing Matthew Parker's Manuscripts](https://iiif.io/event/2019/goettingen/program/39/)
* [Large scale IIIF implementation at Bavarian State Library](https://iiif.io/event/2019/goettingen/program/3/)
* [Getty Common Image Service: Research & Design Report](https://iiif.io/event/2019/goettingen/program/36/)

After a short break, the remaining audience was gathered in one room for the last real session(s), the "IIIF, AI and Machine Learning panel session" where some speakers presented their approaches in the AI field.

## Conclusio

It was a nice conference, really great organized with many international speakers and a great insight in what is going on and what will be the next hot topics in IIIF (AI, AV). Sometimes I wished the topics were a little more technical, about Code and Infrastructure, such as the talks by [ÖNB](https://www.onb.ac.at/) and [BSB](https://www.bsb-muenchen.de/).

It's also remarkable, that our IIIF-Viewer [TIFY](https://github.com/subugoe/tify) gained a lot of attention and that it is used in a lot of projects in the world.

All slides from the conference are freely available on [Google Drive](https://drive.google.com/drive/folders/1zpGUxsu_cvKPilxfyaMTq4Zrsyo3bada).

The 2020 IIIF Conference will take place in Boston, MA. next year.
