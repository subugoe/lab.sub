---
layout: post
current: post
cover: assets/images/sbom-anyone.webp
cover-credits: Copyright © 2022 © OWASP Foundation. All rights reserved.
navigation: True
title: SBOM, anyone?
date: 2022-08-17 13:56
tags: ['sbom', 'sca']
class: post-template
subclass: 'post captcha'
author: hynek
doi: https://doi.org/10.59350/edkvm-wey25
---

> The ability for an organization to generate a complete bill-of-material during continuous integration is one of many maturity indicators. BOMs are increasingly required for various compliance, regulatory, legal, or economic reasons.

([Dependency Track, Best Practices](https://docs.dependencytrack.org/best-practices/))

## Introduction

In May 2021 President of the USA Joe Biden has issued the [Executive Order 14028](https://www.federalregister.gov/documents/2021/05/17/2021-10460/improving-the-nations-cybersecurity) on “Improving the Nation's Cybersecurity”. The document makes several demands on federal agencies concerning the implementation of Cybersecurity and identifies the security and integrity of the software supply chain as one vital claim. But, why would a German university library be interested in proposals made by the [POTUS](https://www.dwds.de/wb/POTUS)?

On December 16, 2021, the Bundesamt für Sicherheit in der Informationstechnik [upgraded its assessment of the Log4Shell threat](https://www.bsi.bund.de/DE/Service-Navi/Presse/Pressemitteilungen/Presse2021/211211_log4Shell_WarnstufeRot.html) to the highest possible level. Although [CVE-2021-44228](https://nvd.nist.gov/vuln/detail/CVE-2021-44228) was already disclosed on December 10, the update of software systems took a huge amount of effort and time, also at the SUB Göttingen.

With a Software Bill of Materials, the identification of vulnerable components, the assessment of risks, and the application of upgrades and patches could have been accelerated substantially.

## What is a Software Bill of Materials (SBOM)?

> An SBOM is a formal, machine-readable inventory of software components and dependencies, information about those components, and their hierarchical relationships. These inventories should be comprehensive – or should explicitly state where they could not be. SBOMs may include open source or proprietary software and can be widely available or access-restricted.

([National Telecommunications and Information Administration, SBOM at a Glance](https://www.ntia.gov/files/ntia/publications/sbom_at_a_glance_apr2021.pdf))

## Introduction of a Dependency Tracking Service

[Forschung und Entwicklung](https://www.sub.uni-goettingen.de/projekte-forschung/forschung-entwicklung/) now maintains an [instance of Dependency Track](https://deps.sub.uni-goettingen.de/) for the use within the SUB. This service has not yet formally been announced but is in a production environment and ready for ingest of your data. The login is secured by the GWDG Academic Cloud SSO and requires the assignment of specific rights to access the portfolio.

[Dependency Track](https://dependencytrack.org/) is currently the only available FLOSS platform for maintaining an inventory of SBOMs and allows for a continuous assessment of security risks in the dependencies of software applications.

It can benefit people who produce, choose, or operate software and could be for a great use within the SUB if adopted throughout the departments who are in any way concerned with software.

## Getting Started (for software developers)

1. [Log in to the platform](https://deps.sub.uni-goettingen.de/) and [contact an administrator](https://gitlab.gwdg.de/subugoe/k8s-central-services/docs/dependency-track.md) to get the required permissions.
1. Start reading the [Dependency Track Documentation on Usage](https://docs.dependencytrack.org/usage/cicd/).
1. Generate your first SBOM with [one of the various tools](https://cyclonedx.org/tool-center/) and inspect it thoroughly.
1. Upload your SBOM to Dependency Track and get yourself familiar with the platform.
1. Gradually integrate SBOM generation and upload into the CI/CD workflow of all your software projects.

## Prospects

Providing an inventory of software components and dependencies enables the organisation to

- create awareness of supply chain security risks,
- implement Software Composition Analysis,
- enforce policies on licenses and security,
- deliver supply chain artifacts to external stakeholders,
- build the ground for an OpenChain assessment and/or certification,

and a lot more.

## Further Reads

- [Steve Springett: Component Analysis](https://owasp.org/www-community/Component_Analysis)
- [CycloneDX Specification Overview](https://cyclonedx.org/specification/overview/)
- [Moving the U.S. Government Toward Zero Trust Cybersecurity Principles](https://zerotrust.cyber.gov/)
- [Stephen Hendrick: Software Bill of Materials (SBOM) and Cybersecurity Readiness](https://linuxfoundation.org/wp-content/uploads/LFResearch_SBOM_Report_020422.pdf)
- [Software Bill of Materials](https://www.ntia.gov/sbom)
