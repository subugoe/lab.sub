---
layout: post
current: post
cover: assets/images/t3doc.webp
cover-credits: David Henry Friston, Public domain, via Wikimedia Commons
navigation: True
title: Documentation with reStructuredText for TYPO3
date: 2021-03-17 04:42:00
tags: ['tools', 'typo3', 'documentation']
class: post-template
subclass: 'post t3doc'
author: ipf
doi: https://doi.org/10.59350/k50bq-tj087
---
First of all - I could have put much more words in the title because we are covering
a complete workflow for our TYPO3 documentation (for our customers and their sites).

We will see the tools used, how it is organized and finally how it is deployed.

## Format

TYPO3 used to use an OpenOffice-based documentation shipped as *.sxw files ages ago. A few years (or a decade) ago,
it was decided to switch to another format and to prefer [reStructuredText](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html) for all core and extension-related documentation.

reStructuredText has its origins in the python-world and is used there for nearly anything.

The [basic markup](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html) is quite similar to what you might
know from markdown. But it has advantages over markdown, such as providing a complete set of 
documentation, a structure where you can include files and have different files for different chapters.
The documentation can be rendered with [Sphinx](https://www.sphinx-doc.org/en/master/index.html) into many formats, such as manpages, LaTeX, HTML, PDF and so on.

It also has its disadvantages, and that's on the one hand the tooling, and on the other hand it's much
more complicated.

As a PHP/TYPO3 developer using Sphinx and reStructuredText feels completely strange and you need a lot 
of stuff installed to get it running somehow.

## Editing and Structure

reStructuredText-files are plain text files and mode IDE provide at least syntax highlighting.

I don't want to bore you with the syntax, everything can be found [here](https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html).

So, we have a TYPO3 site package (I wrote about that in another post), and in this we have a directory called `doc`.

In the root of that directory our main starting-point is a file called Index.rst. In this file we define some include directives
and define our table of contents.

Below that directory everything is organized by topics, such as "Backenduser Administration" or "Adding News" with dedicated *.rst files and directories.

![directory](/assets/images/t3doc-structure.webp)

### Tools

When it comes to tooling, my preferred IDE (JetBrains) is not the best choice, but a colleague mentioned,
that there is also a [Visual Studio Code plugin](https://docs.restructuredtext.net/index.html) for reStructuredText. 

I got it running after solving some issues.

First, we use a [theme](https://sphinx-rtd-theme.readthedocs.io/en/latest/index.html) that has to be installed.
After installing all the python3, pip and sphinx-tools, the next step was to install the theme locally with `pip install sphinx-rtd-theme`.
The theme is defined in the main `config.py` file in a line `extensions = ['sphinx_rtd_theme', 'sphinx.ext.intersphinx', 'sphinx.ext.todo', 'sphinx.ext.ifconfig']` and the option
`html_theme = 'sphinx_rtd_theme'.`

When your environment is ready, this should be all there is to do (except for the `make` and config things that can be found in the repository)
Editing *.rst documentations has never been easier (at least for me).

![image](/assets/images/vscode-rst.webp)

We do also have a docker-container, where the documentation is rendered once, so no live-reload is provided - that's one
of the best features of the Visual Studio Code plugin.

The Dockerfile for rendering the documentation is a short one:

```dockerfile
FROM sphinxdoc/sphinx as build
WORKDIR /docs

COPY ./doc /docs/

RUN pip install sphinx-rtd-theme && \
        cd _make && \
        make html

FROM nginx:alpine

COPY --from=build /docs/_make/build/html/ /usr/share/nginx/html/
RUN rm /usr/share/nginx/html/index.html && \
    ln -s /usr/share/nginx/html/Index.html /usr/share/nginx/html/index.html
```
We use a sphinx-related base image, install our theme and build it. Then we take the built files and copy them
into an nginx-container, that is fired up and contains the static HTML-files from the documentation.

## Deploying

To have the documentation readable by the editors and customers, we take advantage of [Gitlab's pages](https://docs.gitlab.com/ee/user/project/pages/) feature
(a clone of GitHub Pages)

The idea behind this is, that the documentation gets rendered on each commit and automatically gets deployed to the Gitlab webserver.

This is done by having an entry in a `.gitlab-ci.yml` file with the section:

```yaml
pages:
  image: sphinxdoc/sphinx
  before_script:
    - 'true'
  artifacts:
    paths:
      - public
  script:
      - pip install sphinx-rtd-theme
      - cd doc/_make && make html
      - cp -R build/html/* ../../public/
      - cp ../../public/Index.html ../../public/index.html
```

In general, we do exactly the same thing as in the Dockerfile above. The last line is a quirk, because our
main entrypoint in the documentation is an uppercase file `Index.rst` that is not recognized as index-file by
the webserver.

## Conclusion

reStructuredText is a comprehensive tool for writing good documentation, but it's quite hard to get into it.
But when you've come into it, everything will work as a charm.

A TYPO3-theme and some information on how it is used can be found [here](https://typo3-documentation.github.io/sphinx_typo3_theme/readme.html),
documentation for the TYPO3 documentation and installing instructions for sphinx and so on are on [docs.typo3.org](https://docs.typo3.org/m/typo3/docs-how-to-document/master/en-us/WritingReST/Index.html). 
