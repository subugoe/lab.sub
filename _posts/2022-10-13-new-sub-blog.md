---
layout: post
current: post
cover: assets/images/collections-blog.webp
cover-credits: SUB Göttingen https://sub.hypotheses.org/uber
navigation: True
title: New blog from the colleagues
date: 2022-10-13 04:44:44
tags: sustainability
class: post-template
subclass: 'post collections-blog'
author: ipf
doi: https://doi.org/10.59350/904ae-e2v06
---

The colleagues from the other side of the library also started [blogging](https://sub.hypotheses.org/), but more of the book- and collection related library topics.

For hosting the blog, the platform [hypotheses.org](https://hypotheses.org) was chosen. Hypotheses.org mainly focuses on academic content.

Check it out at [https://sub.hypotheses.org/](https://sub.hypotheses.org/) or subscribe to the [RSS Feed](https://sub.hypotheses.org/feed).
