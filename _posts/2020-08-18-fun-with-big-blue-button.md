---
layout: post
current: post
cover: assets/images/bbbfun.webp
cover-credits: Dimitri Torterat, Creative Commons Attribution 2.0 France
navigation: True
title: Fun with Big Blue Button
date: 2020-08-18 04:42:00
tags: ['tools', 'fun']
class: post-template
subclass: 'post bbb-fun'
author: ipf
doi: https://doi.org/10.59350/dd2kd-45n70
---
In our University - and also our Data-Center GWDG we use Open-Source Software whenever it is possible.
So, instead of using proprietary Video Conferencing Services, such as Zoom, Google Meet or Microsoft Teams / Skype, the GWDG provided us a reliable [Big Blue Button](https://bigbluebutton.org/) Service.

This Software is - compared to the proprietary solutions mentioned above - limited in its functionality, especially the fun part!

## Big Blue Button

Big Blue Button is an Open-Source Videoconferencing Software - it claims to be built for Online Learning. It was our go-to solution during the Lockdown-Times here and basically all our Weekly-, Daily- and whatsoever Meetings 
were done using Big Blue Button. In the very beginning we hat to struggle with some issues, but the people at BBB and GWDG did a really good job in fixing Bugs and applying Updates.

As it is aimed at Students and Pupils, all the maybe unnecessary Schnickschnack was not implemented - so there was no possibility to use Custom Backgrounds and have funny Animations running.

## OBS Studio

So, after toying around for some time, I got on the right track and discovered the greade Video Broadcasting Software [OBS Studio](https://obsproject.com/) - Open Broadcaster Software, an Open-Source Software that is used by lots of Influencers and 
Video Streamers around the world. It offers Real-Time streaming and has built-in integrations for a lot of streaming portals, such as YouTube, Twitch and so on.

But I didn't find any Integration for Big Blue Button, but after investigating a little further, I found a few articles describing how to use the output of OBS Studio as a "virtual" Webcam on ones Computer and ended up installing [obs-v4l2sink](https://github.com/CatxFish/obs-v4l2sink).
This only works on Linux Machines, but there are equivalents on Mac and Windows.

One word on the term "virtual webcam" - it is not a physical device, but a device that emulates one. When starting the video in Big Blue Button one is asked which video device should broadcast the video, and instead of the "real" webcam on the computer it broadcasts the video
from the OBS Studio output.

![OBS Studio](assets/images/bbb-fun/obs-scenes.webp)

## Hardware and Integration

### Webcam

Webcams have become quite expensive in the last few month, and the built-in webcam of my Laptop did not meet my requirements. It is a really good one, but as with all Laptop Cameras you always look down. I wanted something that filmed me straigt from the front.
When I thougt about possible solutions, I wondered why it should not be possible to use the best camera I do have available - the camera on my Smartphone.

Again after some research I discovered the App [droidcam](https://www.dev47apps.com/) that has some nifty features. It is used in conjunction with an Desktop Application of the same name which is available for most major platforms.
When the App starts it opens a connection vie HTTP and if the phone is connected via USB-Cable this more reliable connection can be used.

### Integration

Desktop client:

![droidcam Desktop](assets/images/bbb-fun/droidcam-desktop.webp)

Android App:

![droidcam Phone](assets/images/bbb-fun/droidcam-phone.webp)

This droidcam camera can be used as video device for OBS Studio, but also directly for Big Blue Button.

One step further I found the App [droidcam OBS](https://www.dev47apps.com/obs/) that - once started on the phone and given the plugin is installed on the desktop - nicely and directly integrates as a video device in OBS-Studio.

### Camera Holder

The Camera needs to go into a good position, so that you are looking direcly straigt into the camera. For achieving this, I bought [this](https://www.lamicall.com/product/gooseneck-tablet-holder-ls02/) cheap and real stable second hand smartphone holder, that sits directly on the top of the monitor and does a great job.

![droidcam Phone](assets/images/bbb-fun/smartphone-holder.webp)

## Green Screen

Green Screen or [Chroma key](https://en.wikipedia.org/wiki/Chroma_key) is a technique, that every one of us has seen directly or indirectly, when watching the news or any movie with special effects and CGI-Visuals.
Basically it is about having a color, that does not exist in the human nature, that is replaced by any arbitrary image, video and so on. The color green is mostly used for achieving this and so the term "Green Screen" is quite common for Chroma-keying.

I wanted to have fancy backgrounds in Video conferences, so I took an old - really green bedcloth and put that on two microphone stands right behind me, that the scene the camera was filming only contains me an the green cloth.

Front:

![Green Screen Front](assets/images/bbb-fun/cloth-front.webp)

From Behind:

![Green Screen From Behind](assets/images/bbb-fun/cloth-back.webp)

## Backgrounds

Backgrounds for the Green Screen - or for me should be funny - but not too exaggerated. So, a colleague took some photos and also some videos from different perspectives and rooms of the office. Having some moving things in the background provides a far more vivid and realistic experience for the viewer.

In OBS Studio you can combine videos with images and Webcam Video and position everything until it fits. In one test-drive video conference I used an image I found in the [Internet](https://www.curbed.com/2020/3/26/21193949/zoom-background-options-images-home-interior) that shows a really cleaned-up room with a book shelf and so on, but none of the participants wondered about it - so I guess I had a good and realistic setup.

When using one of the colleague's as background videos in a group meeting, some participants thought I was in the office. So, mission accomplished.

## Performance

The broadcasting thing - especially with loads of scenes and videos running in the background is really CPU and Memory Intensive and it may be laggy, so use a 
fast and modern computer for that. Sound and Video may be out of sync if different devices are used - I did not try to route the audio input through OBS-Studio, but that is definetly possible.

## Conclusion

I had a really good time setting up and trying these things and got some insight into the streaming / broadcasting techniques many Youtubers are using. One thing I did not really care about was the lighting - there may be some things I can optimize.

The combination of Software and Hardware I used is really powerful and can also be used for a more professional Video production. Maybe one day I will become a famous Influencer.
